# Doc site template

This makes documentation sites easier on gitlab.

## Local building

Run the following
```
pip install -r requirements.txt
mkdocs serve
```

## Publishing
Automatic with gitlab, enjoy
