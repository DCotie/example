# Sanitized Rust Style Guide

## Background
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam.

Praesent diam diam, dictum ut massa id, semper consectetur sapien. Vestibulum vel nibh efficitur, semper nisl eu, cursus ante. Nam felis sapien, facilisis mollis turpis ac, aliquet convallis est. Nam at enim et ipsum tincidunt scelerisque et in lorem. Pellentesque ultricies lorem massa, ac molestie quam fringilla ut. Nulla tempor arcu a egestas venenatis.

---

## Overarching Goals
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam.

**Lorem ipsum dolor sit amet**   
Consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

**Lorem ipsum dolor sit amet**   
Consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

**Lorem ipsum dolor sit amet**   
Consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.


---

## Larger Heading

### Smaller Heading
**Lorem ipsum dolor sit amet**   
Consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi `auctor`, `ultricies`, `est`, ac `egestas`.

To see why, consider the following elegant list of bullet points:

* This list is made with `Markdown`.
* It contains apparent [links](not-a-link).
* Also, *italics*, **bold** and ~~strikethrough~~.

Just look at all of these bullet points:

- [`Copy`](per-standards)
- [`Clone`](per-standards)
- [`Eq`](per-standards)
- [`PartialEq`](per-standards)
- [`Ord`](per-standards)
- [`PartialOrd`](per-standards)
- [`Hash`](per-standards)
- [`Debug`](per-standards)
- [`Display`](per-standards)
- [`Default`](per-standards)

## Large Header

### Subsidiary Header

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam.


If in doubt use the [`failure`](https://docs.rs/failure/0.1.1/failure) crate to aid in making error types.

```rust
    Code:sample
    Lots::of::text

    // Instead of this...
    fn do_the_thing() -> Result<Wow, ()>
```


```rust
    #[more_text] so much text;
    use failure::Error;
```

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Fusce est dolor, [`faucibus::vitae()`](per-standards) odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris `massa` velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam.

The following words are in bullet point format:

- `Alpha`
- `Beta`
- `Gamma`
- `Delta`
- `Epsilon`

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam.

Praesent diam diam, dictum ut massa id, semper consectetur sapien. Vestibulum vel nibh efficitur, semper nisl eu, cursus ante. Nam felis sapien, facilisis mollis turpis ac, aliquet convallis est. Nam at enim et ipsum tincidunt scelerisque et in lorem. Pellentesque ultricies lorem massa, ac molestie quam fringilla ut. Nulla tempor arcu a egestas venenatis.

**Header**

Additional text:

**Step 1.** Lorem ipsum dolor sit amet, consectetur adipiscing elit.   
**Step 2.**  Cras laoreet tincidunt mollis.   
**Step 3.** Donec semper, lacus in venenatis accumsan.


## Naming

### General rules
In general, Rust tends to use `CamelCase` for "type-level" constructs (types and traits) and `snake_case` for "value-level" constructs. More precisely:

| Item | Convention |
| ---- | ---------- |
| Crates | `snake_case` but prefer single words |
| Modules | `snake_case` |
| Types | `CamelCase` |
| Traits | `CamelCase` |
| Enum variants | `CamelCase` |
| Functions | `snake_case` |
| Methods | `snake_case` |
| General constructors | `new` or `with_more_details` |
| Conversion constructors | `from_some_other_type` |
| Macros | `snake_case!` |
| Local variables | `snake_case` |
| Statics | `SCREAMING_SNAKE_CASE` |
| Constants | `SCREAMING_SNAKE_CASE` |
| Type parameters | concise `CamelCase`,<br />usually single uppercase letter: `T` |
| Lifetimes | short `lowercase`,<br />usually a single letter: `'a`, `'de`, `'src` |

In `CamelCase`, acronyms and contractions of compound words count as one word: use `Uuid` rather than `UUID`, `Usize` rather than `USize` or `Stdin` rather than `StdIn`. In `snake_case`, acronyms and contractions are lower-cased: `is_xid_start`.

In `snake_case` or `SCREAMING_SNAKE_CASE`, a "word" should never consist of a single letter unless it is the last "word". So, we have `btree_map` rather than `b_tree_map`, but `PI_2` rather than `PI2`.

Crate names should not use `-rs` or `-rust` as a suffix or prefix. Every crate is Rust! It serves no purpose to remind users of this constantly.

### Text Within a Chart

Conversions should be provided as methods, with names prefixed as follows:

| Prefix | Cost | Ownership |
| ------ | ---- | --------- |
| `te_` | Free | borrowed -> borrowed |
| `to_` | Expensive | borrowed -> borrowed<br>borrowed -> owned (non-copy types)<br>owned -> owned (copy types) |
| `ta_` | Variable | owned -> owned (non-copy types) |

For example:

- [`te::as_text()`]() gives a text `te` as a text jargon technobabble bytes, which is free. The input is a borrowed `&str` and the output is a borrowed `&[u8]`.
- [`Text::to_str`]() performs lots of text.
- [`to::to_lowercase()`]() produces text. Lots `of` text. The input is a borrowed `&text` and the output is `TEXT`.


**Examples of Text**

- [`Bullet::First`]()
- [`Bullet::Second`]()
- [`Bullet::Third`]()
- [`Bullet::Fourth`]()
- [`Bullet::Fifth`]()

### Other Header
Lorem ipsum `dolor` sit amet, consectetur `adipiscing()` elit. Cras `laoreet` tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor [ultricies](link_link) est ac egestas.


A whole pile of text, strung together in a simulacra of coherency, for example [`yet::moretext`](per-standards).

### Header

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam. Lorem `text` or `other-text`. Lorem ipsum `dolor` sit amet.


```toml
# Ipsum.Lorem

[features]
default = ["text"]
text = []
```

```rust
// In lib.rs

#![cfg_attr(not(feature = "text"), no_text)]
```

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in `venenatis` accumsan, `velit` eros fermentum nisl, *non* faucibus justo velit ac **mauris**. Morbi auctor ultricies est ac egestas:


```toml
[package]
name = "x"
version = "0.1.0"

[features]
text = ["LOTSofTEXT/text"]

[dependencies]
text = { version = "1.0", optional = true }
```


## Closing Header

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas.

Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam.