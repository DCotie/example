# Sanitized Perl Style Guide

## Background

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas. Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam. Praesent diam diam, dictum ut massa id, semper consectetur sapien. Vestibulum vel nibh efficitur, semper nisl eu, cursus ante. Nam felis sapien, facilisis mollis turpis ac, aliquet convallis est. Nam at enim et ipsum tincidunt scelerisque et in lorem. Pellentesque ultricies lorem massa, ac molestie quam fringilla ut. Nulla tempor arcu a egestas venenatis.

---

## The Golden Rule

> Lorem ipsum dolor sit amet

or

> "Lorem ipsum dolor!"  - Unknown
   
---

## Goals of the Style Guide

**Behold, a header**   
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris.

Text, `#!formatted text` multiple additional instances of text. Ever more text. Text.

**Another header**   
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas. Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam. Praesent diam diam, dictum ut massa id, semper consectetur sapien. Vestibulum vel nibh efficitur, semper nisl eu, cursus ante. Nam felis sapien, facilisis mollis turpis ac, aliquet convallis est. Nam at enim et ipsum tincidunt scelerisque et in lorem. Pellentesque ultricies lorem massa, ac molestie quam fringilla ut. Nulla tempor arcu a egestas venenatis.

**Tertiary header**   
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas. Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet. Vivamus ut interdum elit, et blandit quam. Praesent diam diam, dictum ut massa id, semper consectetur sapien. Vestibulum vel nibh efficitur, semper nisl eu, cursus ante. Nam felis sapien, facilisis mollis turpis ac, aliquet convallis est. Nam at enim et ipsum tincidunt scelerisque et in lorem. Pellentesque ultricies lorem massa, ac molestie quam fringilla ut. Nulla tempor arcu a egestas venenatis.


---

## Script Standards

Some text. More text. Formatted text: `#!perl strict` and `#!perl warnings`.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet tincidunt mollis. Donec semper, lacus in venenatis accumsan, velit eros fermentum nisl, non faucibus justo velit ac mauris. Morbi auctor ultricies est ac egestas. Fusce est dolor, faucibus vitae odio nec, imperdiet placerat urna. Praesent tristique turpis ut tempus fringilla. Mauris massa velit, mattis id tortor ultricies, lacinia congue ex. Donec non pharetra risus. Sed sit amet justo lectus. Phasellus imperdiet nunc ipsum, quis sagittis elit mattis sit amet.

For example, the text contains `#!perl no strict` or `#!perl no warnings` in foramtting.  This is text. Lots of sanitized text that provides an **example** of this formatting.

### Organizing `#!text use` statements

Lorem ispum dolor sit `#!text use` amet, consectetur elit..

List `#!text use` lorem alphabetically and group them in this order:

1. Lorem
2. Ipsum dolor
3. Sit amet

---

## Naming

### Format Reference

| Item                        | Convention                            |
| :--------------------------:| :-----------------------------------: |
| Text         | `CamelCase::CamelCase` (but ideally, single word separated by `#!text ::`) |
| Variables                   | `snake_case`                          |
| Constants                   | `SCREAMING_SNAKE_CASE`                |
| Subroutines*                | `snake_case`                          |
| Methods*                    | `snake_case`                          |
| Private variables           | `_snake_case`                         |
| Private text                | `_snake_case`                         |


For more information about text, refer to [this is a link](fake_link_text).


---

## Formatting

### Topic
This contains text about topic. This contains *emphatic* text about topic.


### Secondary Topic
Text relevant to secondary topic. Text. Additional text.

```perl5
if (x == true) {
  solve_x();
}

sub text {
    my $text = @_;
}
```

This applies to all text blocks (`#!text if`, `#!text sub`, `#!text for`, `#!text while`, `#!text do`, `#!text eval`, etc.).

