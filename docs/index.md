# Developer Guide

There are lots of ways to contribute to this program: coding, testing, improving the build process and tools, and contributing to the documentation. This site provides information and references help.

<div class="two-column">
<h3>Documentation</h3>
<div class="divider">
<ul>
  <li>
    <a href="new-dev-checklist"><i class="far fa-user"></i> New developer checklist</a><br />
    Onboarding information for new developers
  </li>

  <li>
    <a href="contributing"><i class="fas fa-hands-helping"></i> Contributing</a><br />
    How to contribute to an existing project
  </li>

  <li>
    <a href="standards"><i class="fas fa-check-circle"></i> Standards</a><br />
    Existing standards for software engineers
  </li>

  <li>
    <a href="build"><i class="fas fa-terminal"></i> Build instructions</a><br />
    How to building software in the tree
  </li>

  <li>
    <a href="running"><i class="fas fa-cogs"></i> Running artifacts</a><br />
    How to run artifacts from the tree 
  </li>

  <li>
    <a href="develop"><i class="fas fa-desktop"></i> Dev Env Setup</a><br />
    How to set up development tools and environments
  </li>

  <li>
    <a href="source-tree"><i class="fas fa-code-branch"></i> Working with Source Tree</a><br />
    Code overviews
  </li>

  <li>
    <a href="testing"><i class="fas fa-bug"></i> Automated Testing</a><br />
    How to write and run tests
  </li>

  <li>
    <a href="debugging"><i class="fas fa-wrench"></i> Debugging</a><br />
    How to successfully squash bugs
  </li>
</ul>
</div>

</div>

<div class="two-column">
<h3 style="padding-left:4px">Tools</h3>
<ul>
  <li>
    <a href="https://contegixapp1.livenation.com/jira/secure/RapidBoard.jspa?projectKey=CTH&rapidView=3589&view=planning"><i class="fas fa-tasks"></i> Jira</a><br/>
    Issue and task tracker
  </li>

  <li>
    <a href="tools/git"><i class="fab fa-git"></i> Git</a><br />
    Version control system
  </li>

  <li>
    <a href="tools/git-workflow"><i class="fab fa-git"></i> Git Workflow</a><br />
    Forking and merging within our workflow
  </li>
  
  <li>
    <a href="tools/gitlab"><i class="fab fa-gitlab"></i> Gitlab</a><br />
    Automated manager for code review
  </li>
  
  <li>
    <a href="tools/ci"><i class="fab fa-gitlab"></i> Gitlab-CI</a><br />
    CI/CD suite
  </li>

  <li>
    <a href="tools/bazel"><i class="fas fa-terminal"></i> Bazel</a><br />
    Build tooling
  </li>

  <li>
    <a href="tools/kubernetes"><i class="fas fa-cubes"></i> Kubernetes</a><br />
    Understanding and using Kubernetes
  </li>

  <li>
    <a href="https://sonar.com"><i class="fas fa-thermometer-three-quarters"></i> Sonar</a><br />
    Tracking the trees code quality
  </li>

  <li>
    <a href="http://slack.com"><i class="fab fa-slack-hash"></i> Slack</a><br />
    Tool for team and project communication
  </li>
</ul>
</div>
