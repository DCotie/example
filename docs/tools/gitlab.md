# Gitlab

We share and cross collaborate all of our repos via gitlab. This is a repo management tool, and whilst it is not required for successful usage of [git](./git) it does provide extra functionality such as pull requests.

We generally use a fork-and-merge model as explained below

## Project forking & merge workflow
Forking a project to your own namespace is useful to work on the project you want to contribute to. 

![protip](../imgs/patch.png)

### Creating a fork
Forking a project is in most cases a two-step process.

* Click on the fork button located in the middle of the page or a project's home page right next to the stars button.

![the-big-form-button](../imgs/gitlab-forking_workflow_fork_button.png)

* Once you do that, you'll be presented with a screen where you can choose the namespace to fork to. You are advised to choose your own namespace. Click on the namespace to create your fork there.

![choose-your-namespace](../imgs/gitlab-forking_workflow_choose_namespace.png)

### Creating a Pull Request
Often in git you will here the idea of pulling from a tree or creating a merge request.

This is the principal barrier where code reviews happen. For example the [linux kernel](https://kernel.org) has a mailing list where the following warcry is often heard

!!! quote
    ```
    Hi Linus,

    Please pull powerpc updates for 4.6:

    The following changes since commit 9ab3ac233a8b4ffcc27c8475b83dee49fc46bc76:

      powerpc/mm/hash: Clear the invalid slot information correctly (2016-02-22 19:27:39 +1100)

    are available in the git repository at:

      git://git.kernel.org/pub/scm/linux/kernel/git/powerpc/linux.git tags/powerpc-4.6-1

    for you to fetch changes up to 6e669f085d595cb6053920832c89f1a13067db44:

      powerpc: Fix unrecoverable SLB miss during restore_math() (2016-03-16 15:23:02 +1100)
    ```

For our work we use Gitlab which provides this supporting infrastructure. In our case rather than a mailing list the Merge Request functionality of Gitlab is used instead.

Merge requests are useful to integrate separate changes that you've made to a project, on different branches. This is a brief guide on how to create a merge request. For more information, check the [merge requests documentation](https://docs.gitlab.com/ee/user/project/merge_requests/index.html).

* Before you start, you should have already created a [fork](#creating-a-fork) and, well, actually made some changes!
* Go to the project where you'd like to merge your changes and click on the **Merge requests** tab.
* Click on **New merge request** on the right side of the screen.
* From there on, you have the option to select the source branch and the target branch you'd like to compare to. The default target project is the upstream repository, but you can choose to compare across any of its forks.

![new-merge-req](../imgs/gitlab-merge_request_select_branch.png)

* When ready, click on the Compare branches and continue button.
* At a minimum, add a title and a description to your merge request. Optionally, select a user to review your merge request and to accept or close it. You may also select a milestone and labels.

!!! tip
    ProTip: If you are using [descriptive commit messages](../standards/branch-model#commit-clearly) Gitlab will often scrape these messages into your freshly created pull request.

![merge-details](../imgs/gitlab-merge_request_page.png)

* When ready, click on the **Submit merge request** button.
