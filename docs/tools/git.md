# Git

## Installing Git

Users must have:

* This is filler text.
* And more filler text, containing `formatting`.

Install Git through the appropriate channel for your operating system. For Linux, consult your package manager for the best approach.

Additional reference material can be found at [this]() link.

## Another Sanitized Header
Sanitized text, mostly free of formatting.

More text, with formatting [as is visible here]().

Run this command:

```sh
text text command text
```


## Other Configuration Instructions

### Username and email 
Ensure your username and email address is correctly configured. Under no circumstances should you use a personal or insecure address.

### Merge program
After installing, choose a [merge program](https://git-scm.com/docs/git-mergetool). Seriously. Do it now. If you don't, Git will choose one for you and your entire workstation will catch on fire.
