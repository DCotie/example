# New Developer Checklist

Welcome! This is a reference list to get you started as rapidly as possible.

1. Read through the [contribution guide](../contributing).
2. Install [git and git-lfs](../tools/git).
3. Setup the [build tools](../build).
4. While compiling, read through the excellent [standards](../standards) guidelines.
5. Configure your [dev-environment](../develop) if you are working on contributing code.
6. Get familiar with [running code](../running).
7. Study up on [Source Tree](../source-tree).
8. Familiarise yourself with [testing processes](../testing).
